# MA8701-data-analysis

This repository contains the IPython notebook, dataset and report used in the [data analysis assignment](https://wiki.math.ntnu.no/ma8701/2023v/assignmentsda1) for the course MA8701 (Advanced statistical methods in inference and learning) at NTNU.

To use the IPython notebook `data_analysis.ipynb`, clone this repository, and install the necessary dependencies (as seen in the "imports" cell at the beginning of the notebook) using your preferred method.
